package sda.bogdan.sda.gym.service;

import sda.bogdan.sda.gym.dao.TrainerDao;
import sda.bogdan.sda.gym.model.Trainer;

import java.util.List;

public class TrainerService {

    private TrainerDao trainerDao = new TrainerDao();
    //CRUD Operations  + bussiness logic


    public List<Trainer> getAllTrainers() {
        List<Trainer> allTrainers = trainerDao.getAllTrainers();
        return allTrainers;
    }

    public Trainer getOldestTrainer() {
        List<Trainer> allTrainers = trainerDao.getAllTrainer();
        Trainer oldestTrainer = allTrainers.get(0);
        for (Trainer trainer : allTrainers) {
            //   if (trainer.getAge() > oldestTrainer.getAge()) {
            //   oldestTrainer = trainer;
        }

        return oldestTrainer;
    }
}