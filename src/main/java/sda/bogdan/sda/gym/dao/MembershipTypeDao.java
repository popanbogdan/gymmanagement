package sda.bogdan.sda.gym.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import sda.bogdan.sda.gym.model.MembershipType;
import sda.bogdan.sda.gym.model.Trainer;

import java.util.List;

public class MembershipTypeDao {

    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();


    public List<MembershipType> getMembershipTypes() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Query<MembershipType> query = session.createQuery("from MembershipType");
        List<MembershipType> membershipTypes = query.list();
        tx.commit();
        session.close();

        return null;


        //SessionsFactory
        //CRUD operations, methods use Session and Tranzaction    Query and DB using Sql, Hql


        //getAllTrainers()


    }

    public List<MembershipType> getAllMembershipType() {
        return null;
    }








}
