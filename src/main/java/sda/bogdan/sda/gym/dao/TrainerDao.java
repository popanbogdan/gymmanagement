package sda.bogdan.sda.gym.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import sda.bogdan.sda.gym.model.Trainer;

import java.util.List;

public class TrainerDao {
    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();


    public List<Trainer> getAllTrainers() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Query<Trainer> query = session.createQuery("from Trainer");
        List<Trainer> trainers = query.list();
        tx.commit();
        session.close();

        return null;


        //SessionsFactory
        //CRUD operations, methods use Session and Tranzaction    Query and DB using Sql, Hql


        //getAllTrainers()


    }

    public List<Trainer> getAllTrainer() {
        return null;
    }
    //getAlltrainersById(int triner)
    //createTrainer(Trainer trainer)
    //deleteTrainer(Trainer trainer)
    //deleteTrainer(int trainerId);
}