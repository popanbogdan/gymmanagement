package sda.bogdan.sda.gym.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import sda.bogdan.sda.gym.model.Client;
import sda.bogdan.sda.gym.model.Trainer;

import java.util.List;

public class ClientDao {

    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public List<Client> getAllClients() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Query<Client> query = session.createQuery("from client");
        List<Client> Clients = query.list();
        tx.commit();
        session.close();
        return null;
    }
    }